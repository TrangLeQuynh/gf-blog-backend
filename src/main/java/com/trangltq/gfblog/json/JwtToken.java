package com.trangltq.gfblog.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class JwtToken implements Serializable {

    private String accessToken;

    public  JwtToken() {}

    public JwtToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
