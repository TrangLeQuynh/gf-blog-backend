package com.trangltq.gfblog.json;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class ChatMessageData {
    private Long id;
    private String chatId;
    private Long senderId;
    private String senderName;
    private Long recipientId;
    private String content;
}
