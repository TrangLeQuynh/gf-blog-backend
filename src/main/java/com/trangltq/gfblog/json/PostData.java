package com.trangltq.gfblog.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
public class PostData implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("userId")
    private Long user;

    @JsonProperty("content")
    private String content;

    @JsonProperty("hashTag")
    private String hashTag;

    @JsonProperty("images")
    private List<String> images;

    @JsonProperty("latitude")
    private  double latitude;

    @JsonProperty("longitude")
    private double longitude;

    @JsonProperty("createAt")
    private Timestamp createAt;

    @JsonProperty("updateAt")
    private Timestamp updateAt;

    public PostData() {}

}
