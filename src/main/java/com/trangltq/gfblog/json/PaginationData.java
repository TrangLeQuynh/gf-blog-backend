package com.trangltq.gfblog.json;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class PaginationData {
    Integer totalPages;
    Integer size;
    Integer page;
}
