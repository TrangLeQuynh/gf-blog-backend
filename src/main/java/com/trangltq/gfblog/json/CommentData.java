package com.trangltq.gfblog.json;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
@Getter
@Setter
public class CommentData implements Serializable {

    Long userId;
    Long postId;
    String content;

    public CommentData() {}

    public CommentData(Long userId, Long postId, String content) {
        this.userId = userId;
        this.postId = postId;
        this.content = content;
    }

}
