package com.trangltq.gfblog.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseData implements Serializable {
    private boolean status;
    private Object data;
    private String message;

    public ResponseData() {}

    public ResponseData(boolean status, Object data, String message) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

}
