package com.trangltq.gfblog.json;

import lombok.Data;

@Data
public class FriendData {

    Long userId;
    Long friendId;
    Boolean status;

    public FriendData() {}

    public FriendData(Long userId, Long friendId, boolean status) {
        this.userId = userId;
        this.friendId = friendId;
        this.status = status;
    }
}
