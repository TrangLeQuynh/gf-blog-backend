package com.trangltq.gfblog.json;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class PayloadData {
    Integer type;
    Long id;
    String senderName;
    String description;
}
