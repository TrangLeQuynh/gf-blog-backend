package com.trangltq.gfblog.json;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ImagesData implements Serializable {

    List<String> images;
    public ImagesData() {}

    public ImagesData(List<String> images) {
        this.images = images;
    }

}
