package com.trangltq.gfblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GfBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(GfBlogApplication.class, args);
	}

}
