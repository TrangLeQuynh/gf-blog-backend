package com.trangltq.gfblog.utils;

import java.text.DecimalFormat;
import java.util.Random;

abstract public class UtilConstants {

    static public String verifyCode() {
        return new DecimalFormat("000000")
                .format(new Random().nextInt(999999));
    }

}
