package com.trangltq.gfblog.service;

import com.trangltq.gfblog.model.ChatRoom;
import com.trangltq.gfblog.repository.ChatRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ChatRoomService {

    @Autowired
    private ChatRoomRepository chatRoomRepository;

    public Optional<String> getChatId(
        Long senderId,
        Long recipientId,
        boolean createIfNotExist
    ) {
        Optional<String> res = chatRoomRepository.findBySenderIdAndRecipientId(senderId, recipientId);
        if (res.isPresent()) {
            return res;
        }
        //not exits
        if(!createIfNotExist) {
            return  Optional.empty();
        }
        String chatId = String.format("%d_%d", senderId, recipientId);
        chatRoomRepository.addChatRoom(chatId, senderId, recipientId, false, LocalDateTime.now());
        chatRoomRepository.addChatRoom(chatId, recipientId, senderId, true, LocalDateTime.now());
        return Optional.of(chatId);
    }

    public List<ChatRoom> findChatRooms(Long userId) {
        List<ChatRoom> li = chatRoomRepository.findChatRooms(userId);
        return li;
    }

    public void updateStatue(Long senderId, Long recipientId, Boolean status) {
        chatRoomRepository.updateStatus(senderId, recipientId, status, LocalDateTime.now());
    }

    public void seenMessage(Long senderId, Long recipientId, Boolean status) {
        chatRoomRepository.updateSeenStatus(senderId, recipientId, status);
    }

}
