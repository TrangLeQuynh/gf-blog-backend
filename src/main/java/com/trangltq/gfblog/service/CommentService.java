package com.trangltq.gfblog.service;

import com.trangltq.gfblog.model.Comment;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentReps;


    public void addComment(Long postId, Long userId, String content) {
        LocalDateTime _current =  LocalDateTime.now();
        commentReps.addComment(
            postId,
            userId,
            content,
            _current,
            _current
        );
    }

    public void deleteCommentByID(Long commentId) {
        commentReps.deleteCommentByID(commentId);
    }

    public List<Comment> getListComment(Long postId) {
        return commentReps.getCommentList(postId);
    }

}
