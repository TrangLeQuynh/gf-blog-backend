package com.trangltq.gfblog.service;

import com.trangltq.gfblog.json.ChatMessageData;
import com.trangltq.gfblog.model.ChatMessage;
import com.trangltq.gfblog.repository.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChatMessageService {

    @Autowired
    private ChatMessageRepository chatMessageReps;

    @Autowired
    private ChatRoomService chatRoomService;

    public ChatMessageData save(ChatMessageData chatMessage) {
        chatMessageReps.addChatMessage(
            chatMessage.getChatId(),
            chatMessage.getSenderId(),
            chatMessage.getRecipientId(),
            chatMessage.getContent(),
            LocalDateTime.now()
        );
        chatRoomService.updateStatue(chatMessage.getRecipientId(), chatMessage.getSenderId(), true);
        chatRoomService.updateStatue(chatMessage.getSenderId(), chatMessage.getRecipientId(), false);
        return chatMessage;
    }

    public List<ChatMessage> findChatMessages(Long senderId, Long recipientId) {
        Optional<String> chatId = chatRoomService.getChatId(senderId, recipientId, false);
        if (!chatId.isPresent()) return new ArrayList<>();
        List<ChatMessage> messages = chatMessageReps.findAllByChatId(chatId.get());
        return messages;
    }

}
