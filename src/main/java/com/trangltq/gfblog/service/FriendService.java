package com.trangltq.gfblog.service;

import com.trangltq.gfblog.model.Friend;
import com.trangltq.gfblog.repository.FriendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class FriendService {

    @Autowired
    private FriendRepository friendReps;

    public List<Friend> getListRequest(Long friendId, String status) {
        return friendReps.getRequestFriend(friendId, status);
    }

    public List<Friend> getListFriend(Long userId, String status, String usernameSearch) {
        return friendReps.getListFriend(userId, status, usernameSearch);
    }

    public Friend save(Friend friend) {
        return friendReps.save(friend);
    }

    public void responseRequestFriend(Long userId, Long userFriend, String status) {
        friendReps.updateStatusFriend(userId, userFriend, status);
    }

    public void addFriend(Long userId, Long userFriend, String status) {
        friendReps.addFriend(
            userId,
            userFriend,
            LocalDateTime.now(),
            status
        );
    }

    public void deleteFriend(Long userId, Long userFriend) {
        System.out.print("delete friend " + userId +  " - "+ userFriend );
        friendReps.deleteFriend(userId, userFriend);
    }

    public Integer accountFriends(Long userId, String status) {
        List friend = friendReps.accountFriends(userId, status);
        return friend.size();
    }


}
