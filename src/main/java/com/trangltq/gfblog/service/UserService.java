package com.trangltq.gfblog.service;

import com.trangltq.gfblog.repository.ImageRepository;
import com.trangltq.gfblog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.trangltq.gfblog.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VerificationService verificationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ImageRepository imageReps;

    public Optional<User> existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public Optional<User> findByUsernameOrEmail(String account) {
        return userRepository.findByUsernameOrEmail(account);
    }

    public List<User> findAllByUsername(String username){
        return userRepository.findAllByUsername(username);
    }

    public Optional<User> getUserByUsername(String username){
        return userRepository.getByUsername(username);
    }

    public Optional<User> getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    public User addUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        user.setCreateAt(currentTime);
        user.setUpdateAt(currentTime);
        return userRepository.save(user);
    }

    public User editUser(User user) {
        return userRepository.save(user);
    }


    public Optional<User> checkPassword(String username, String oldPassword) {
        return userRepository.checkOldPassword(username, passwordEncoder.encode(oldPassword));
    }

    public boolean deleteUser(Long id) {
        userRepository.deleteById(id);
        return true;
    }

    public List<User> findNewFriend(Long userId, String searchText) {
        return userRepository.findNewFriend(userId, searchText);
    }

    public boolean updateAvatar(Long userId, String avatar) {
        try {
            imageReps.saveImage(
                avatar,
                userId,
                "",
                0,
                LocalDateTime.now()
            );
            userRepository.updateAvatar(userId, avatar);
            return true;
        } catch (Exception e) {
            return  false;
        }
    }

    public boolean updateCover(Long userId, String cover) {
        try {
            imageReps.saveImage(
                cover,
                userId,
                "",
                1,
                LocalDateTime.now()
        );
            userRepository.updateCover(userId, cover);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public User getUserById(Long id) {
        try {
            return userRepository.findById(id).get();
        } catch (Exception e) {
            return  null;
        }
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    @Transactional
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username).get();
    }
}
