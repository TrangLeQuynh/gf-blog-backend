package com.trangltq.gfblog.service;

import com.trangltq.gfblog.model.LanguageConfig;
import com.trangltq.gfblog.model.Languages;
import com.trangltq.gfblog.repository.LanguageConfigRepository;
import com.trangltq.gfblog.repository.LanguagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LanguageService {

    @Autowired
    private LanguageConfigRepository languageConfigReps;

    @Autowired
    private LanguagesRepository languagesReps;

    public List<LanguageConfig> getLanguageConfigList() {
        List<LanguageConfig> li = languageConfigReps.findAll();
        return  li;
    }

    public Map<String, String> getLanguages(String languageCode) {
        List<Languages> li;
        if (languageCode.compareTo("vi") == 0)
            li = languagesReps.getLanguagesVI();
        else
            li = languagesReps.getLanguagesEN();
        Map<String, String> res = new HashMap<>();
        for (Languages item: li) {
            res.put(item.getKeyWord(), item.getTrans());
        }
        return res;
    }


}
