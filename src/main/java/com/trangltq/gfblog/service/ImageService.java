package com.trangltq.gfblog.service;

import com.trangltq.gfblog.model.Image;
import com.trangltq.gfblog.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageReps;

    public void saveImage(Image image) {
        imageReps.saveImage(
            image.getId(),
            image.getUserId(),
            image.getDescription(),
            image.getType(),
            LocalDateTime.now()
        );
    }

    public void deleteImage(String id) {
        imageReps.deleteById(id);
    }

    public List<String> getListImageByUserID(Long userId, Integer page, Integer size) {
        try {
            List<String> li =  imageReps.findAllByUserId(userId, page * size, size);
            return li;
        } catch (Exception e) {
            return null;
        }
    }
}
