package com.trangltq.gfblog.service;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.trangltq.gfblog.model.FileUpload;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

@Service
public class S3StorageService {

    @Value("${application.bucket.name}")
    private String bucketName;

    @Autowired
    private AmazonS3 s3Client;

    public String uploadFile(FileUpload fileUpload) {
        byte[] imgBytes = Base64.decodeBase64(fileUpload.getData());
        InputStream stream  = new ByteArrayInputStream(imgBytes);
        String fileName =  UUID.randomUUID() + fileUpload.getFileName();
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(imgBytes.length);
        metadata.setContentType("image/" + fileUpload.getFormat());
        metadata.setCacheControl("public, max-age=31536000");
        s3Client.putObject(new PutObjectRequest(bucketName, fileName,stream, metadata));
        return fileName;
    }

    public String uploadImage(String base64) {
        try {
            String type = base64.substring(base64.indexOf("/") + 1, base64.indexOf(";"));
            byte[] imgBytes = Base64.decodeBase64(base64.substring(base64.indexOf(",") + 1));
            InputStream stream  = new ByteArrayInputStream(imgBytes);
            String fileName =  UUID.randomUUID() + String.valueOf(System.currentTimeMillis()) + "." + type;
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(imgBytes.length);
            metadata.setContentType("image/" + type);
            metadata.setCacheControl("public, max-age=31536000");
            s3Client.putObject(new PutObjectRequest(bucketName, fileName,stream, metadata));
            return fileName;
        } catch (Exception e) {
            return null;
        }
    }

    public byte[] downloadFile(String fileName) {
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte[] content = IOUtils.toByteArray(inputStream);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void deleteFile(String fileName) {
        try {
            s3Client.deleteObject(bucketName, fileName);
        } catch (SdkClientException e) {
            System.out.println("delete fail " + fileName);
        }
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertFile = new File(file.getOriginalFilename());
        try (FileOutputStream fileOutputStream = new FileOutputStream(convertFile)){
            fileOutputStream.write(file.getBytes());
        } catch (IOException e) {
            System.out.println("Error converting multipart file to file" + e.getMessage());
        }
        return convertFile;
    }

}
