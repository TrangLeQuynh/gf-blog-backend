package com.trangltq.gfblog.service;

import com.trangltq.gfblog.json.PostData;
import com.trangltq.gfblog.model.Image;
import com.trangltq.gfblog.model.Post;
import com.trangltq.gfblog.repository.CommentRepository;
import com.trangltq.gfblog.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    @Autowired
    private PostRepository postReps;

    @Autowired
    private S3StorageService s3StorageService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private CommentRepository commentReps;

    public Post savePost(Post post) {
        return postReps.save(post);
    }

    public Optional<Post> getDetailPost(Long id) {
        return postReps.getPostDetail(id);
    }

    public List<Post> getPostsByUserId(Long userId, Integer page, Integer size) {
        return postReps.getPostsByUserId(userId, page*size, size);
    }

    public List<Post> getPostList(Long userId, Integer page, Integer size) {
        return postReps.getPostListAll(userId, "friend", page*size, size);
    }

    public Integer countPost(Long userId) {
        return postReps.countPost(userId);
    }


    public void addPost(Long userId, PostData post) {
        //upload list image
        String images = "";
        List<String> liImage = post.getImages();
        if (liImage != null && liImage.size() != 0) {
            for (String item : liImage) {
                String link = s3StorageService.uploadImage(item);
                imageService.saveImage(
                    new Image(link, userId, "", 2)
                );
                if (link != null) {
                    images += link + ",";
                }
            }
        }
        postReps.addPost(
            userId,
            (String) post.getContent(),
            "",
            images,
            LocalDateTime.now(),
            LocalDateTime.now(),
            (double) post.getLatitude(),
            (double) post.getLongitude()
        );
    }

    public void updatePost(Long userId, PostData post) {
        String images = "";
        List<String> oldImages;
        try {
            oldImages = postReps.getPostDetail(post.getId()).get().getArrayImages();
        } catch (Exception e) {
            oldImages = new ArrayList<>();
        }
        System.out.println("oldImages.size() = " + oldImages.size());
        List<String> liImage = post.getImages();
        int index = 0;
        if (liImage != null && liImage.size() != 0) {
            for (String item : liImage) {
                if (item.contains(":")) {
                    String link = s3StorageService.uploadImage(item);
                    if (link != null) {
                        images += link + ",";
                        imageService.saveImage(
                            new Image(link, userId, "", 2)
                        );
                    }
                    continue;
                }
                images += item + ",";
                for(int i = index; i < oldImages.size(); ++i) {
                    index = i;
                    if (item.equals(oldImages.get(i))) {
                        oldImages.remove(i);
                        break;
                    }
                }
            }
        }
        System.out.println("start to delete");
        deleteListOldImages(oldImages);
        postReps.updatePost(
            post.getId(),
            post.getContent(),
            images,
            LocalDateTime.now(),
            post.getLatitude(),
            post.getLongitude()
        );
    }

    public boolean deletePost(Long id) {
        try {
            commentReps.deleteComment(id);
            Post oldPost = postReps.getPostDetail(id).get();
            for(String image : oldPost.getArrayImages()) {
                s3StorageService.deleteFile(image);
                imageService.deleteImage(image);
            }
            postReps.deletePost(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void deleteListOldImages(List<String> oldImages) {
        System.out.println("oldImages.size() need to delete= " + oldImages.size());
        for (String item : oldImages) {
            s3StorageService.deleteFile(item);
            imageService.deleteImage(item);
        }
    }
}
