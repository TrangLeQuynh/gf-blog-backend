package com.trangltq.gfblog.service;

import com.trangltq.gfblog.model.Verification;
import com.trangltq.gfblog.repository.VerificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VerificationService {

    @Autowired
    private VerificationRepository verificationReps;

    public Optional<Verification> checkVerifyCode(Long id, String verifyCode) {
        return verificationReps.checkVerification(id, verifyCode);
    }

    public void save(Verification verification) {
        verificationReps.save(verification);
    }

}
