package com.trangltq.gfblog.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
public class FriendKey implements Serializable {

    @NotNull
    @OneToOne
    @JoinColumn(name = "user_id")
    @JsonProperty("user")
    private User userId;

    @NotNull
    @OneToOne
    @JsonProperty("friend")
    @JoinColumn(name = "friend_id")
    private User friendId;

    public FriendKey(){}

    public FriendKey(User userId, User friendId) {
        this.userId = userId;
        this.friendId = friendId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public void setFriendId(User friendId) {
        this.friendId = friendId;
    }

    public User getUserId() {
        return userId;
    }

    public User getFriendId() {
        return friendId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FriendKey)) return false;
        FriendKey that = (FriendKey) o;
        return Objects.equals(getUserId(), that.getUserId()) &&
                Objects.equals(getFriendId(), that.getFriendId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getFriendId());
    }

}
