package com.trangltq.gfblog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "friend")
@Data
public class Friend {

    @EmbeddedId
    private FriendKey id;

    @Column(name = "status")
    @JsonProperty("status")
    private String status;

    @Column(name = "create_at")
    @JsonProperty("createAt")
    private Timestamp createAt;

    public Friend(FriendKey id, String status) {
        this.id = id;
        this.status = status;
    }

    public Friend() {
    }

    public void setId(FriendKey id) {
        this.id = id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    public String getStatus() {
        return status;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public FriendKey getId() {
        return id;
    }
}
