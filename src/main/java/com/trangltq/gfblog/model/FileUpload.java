package com.trangltq.gfblog.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileUpload {
    private String fileName;
    private String data;
    private String format;
}
