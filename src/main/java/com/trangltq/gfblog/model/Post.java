package com.trangltq.gfblog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = "post")
@Data
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonProperty("id")
    private Long id;

    @JsonProperty("user")
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @Column
    @JsonProperty("content")
    private String content;

    @Column(name = "hash_tag")
    @JsonProperty("hashTag")
    private String hashTag;

    @Column
    private String images;

    @Column
    @JsonProperty("latitude")
    private  double latitude;

    @Column
    @JsonProperty("longitude")
    private double longitude;

    @Column(name = "create_at")
    @JsonProperty("createAt")
    private Timestamp createAt;

    @Column(name = "update_at")
    @JsonProperty("updateAt")
    private Timestamp updateAt;

    public Post() {}

    public Post(User user, String content, String hashTag, String images) {
        this.user = user;
        this.content = content;
        this.hashTag = hashTag;
        this.images = images;
        this.updateAt = new Timestamp(System.currentTimeMillis());
    }

    @JsonProperty("images")
    public List<String> getArrayImages() {
        if (images == null)
            return new ArrayList<>();
        String[] li = images.split(",");
        return Arrays.asList(li).stream()
            .filter(item -> !item.equals(""))
            .collect(Collectors.toList());
    }
}
