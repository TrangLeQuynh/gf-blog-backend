package com.trangltq.gfblog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
@Setter
@Data
@Entity
@Table(name = "image")
public class Image {

    @Id
    @Column(name = "id")
    private String id;

    @Column
    @JoinColumn(name = "user_id")
    private Long userId;

    @Column
    private String description;

    @Column
    private Integer type;

    @Column(name = "create_at")
    @JsonProperty("createAt")
    private Timestamp createAt;

    public Image() {
    }

    public Image(String id, Long userId, String description, Integer type) {
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.type = type;
    }
}
