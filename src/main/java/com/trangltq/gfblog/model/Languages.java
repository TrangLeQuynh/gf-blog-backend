package com.trangltq.gfblog.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "languages")
@Data
public class Languages {

    @Id
    @Column(name = "key_word")
    @GeneratedValue
    String keyWord;

    String trans;
}
