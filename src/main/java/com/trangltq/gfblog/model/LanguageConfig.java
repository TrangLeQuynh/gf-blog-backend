package com.trangltq.gfblog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "language_config")
@Data
public class LanguageConfig {

    @Id
    @Column
    @GeneratedValue
    @JsonProperty("code")
    String code;

    @Column
    @JsonProperty("name")
    String name;

    @Column
    @JsonProperty("flag")
    String flag;

    @Column
    @JsonProperty("status")
    Boolean status;

    public LanguageConfig() {}

    public LanguageConfig(String code, String name, String flag, Boolean status) {
        this.code = code;
        this.name = name;
        this.flag = flag;
        this.status = status;
    }

}
