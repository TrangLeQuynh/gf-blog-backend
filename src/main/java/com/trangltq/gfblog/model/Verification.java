package com.trangltq.gfblog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trangltq.gfblog.utils.UtilConstants;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "verification")
public class Verification{
    private static final int EXPIRATION = 10;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @JsonProperty("id")
    private Long id;

    @Column(name = "verify_code")
    @JsonProperty("verify_code")
    private String verifyCode;

    @Column
    @JsonProperty("expire")
    private LocalDateTime expire;

    @JsonProperty("user")
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    public void setId(Long id) {
        this.id = id;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public void setExpire(LocalDateTime expire) {
        this.expire = expire;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public LocalDateTime getExpire() {
        return expire;
    }

    public User getUser() {
        return user;
    }

    public Verification() {}

    public Verification(User user) {
        this.user = user;
        expire = LocalDateTime.now().plusMinutes(EXPIRATION);
        verifyCode = UtilConstants.verifyCode();
    }

    public Boolean isExpired() {
        return getExpire().isBefore(LocalDateTime.now());
    }

}
