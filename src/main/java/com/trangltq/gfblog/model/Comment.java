package com.trangltq.gfblog.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Data
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonProperty("id")
    private Long id;

    @Column(name = "post_id")
    @JsonProperty("postId")
    private Long postId;

    @JsonProperty("user")
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @Column
    @JsonProperty("content")
    private String content;

    @Column(name = "create_at")
    @JsonProperty("createAt")
    private Timestamp createAt;

    @Column(name = "update_at")
    @JsonProperty("updateAt")
    private Timestamp updateAt;

    public Comment() {}

    public Comment(Long postId, User user, String content) {
        this.postId = postId;
        this.content = content;
        this.user = user;
    }


}
