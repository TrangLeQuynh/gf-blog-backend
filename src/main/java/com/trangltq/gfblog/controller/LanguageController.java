package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.LanguageConfig;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/language")
public class LanguageController {

    @Autowired
    private LanguageService languageService;


    @GetMapping(value = "/config")
    public ResponseEntity<?> getYourImages(@AuthenticationPrincipal User userDetails) {
        try {
            List<LanguageConfig> li = languageService.getLanguageConfigList();
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    li,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }

    @GetMapping(value = "/{languageCode}")
    public ResponseEntity<?> getLanguage(@PathVariable String languageCode) {
        try {
            Map<String, String> li = languageService.getLanguages(languageCode);
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    li,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }

}
