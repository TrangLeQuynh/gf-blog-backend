package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.ChatMessageData;
import com.trangltq.gfblog.json.PayloadData;
import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.ChatMessage;
import com.trangltq.gfblog.model.ChatRoom;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.service.ChatMessageService;
import com.trangltq.gfblog.service.ChatRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ChatController {

    @Autowired
    private ChatRoomService chatRoomService;

    @Autowired
    private ChatMessageService chatMessageService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/chat")
    public void processMessage(@Payload ChatMessageData chatMessage) {
        Optional<String> chatId = chatRoomService.getChatId(
            chatMessage.getSenderId(),
            chatMessage.getRecipientId(),
            true
        );
        chatMessage.setChatId(chatId.get());
        ChatMessageData saved = chatMessageService.save(chatMessage);
        //convert payload
        PayloadData _newPayload = new PayloadData();
        _newPayload.setSenderName(chatMessage.getSenderName());
        _newPayload.setId(chatMessage.getSenderId());
        _newPayload.setType(1);
        messagingTemplate.convertAndSendToUser(
            chatMessage.getRecipientId().toString(),
            "/chatroom-list",
            _newPayload
        );
        messagingTemplate.convertAndSendToUser(
            chatMessage.getRecipientId().toString(),
            "/messages",
            saved
        );
    }

    @GetMapping("/messages/{senderId}")
    public ResponseEntity<?> findChatMessages (@PathVariable Long senderId,
                                               @AuthenticationPrincipal User recipient) {
        chatRoomService.seenMessage(recipient.getId(), senderId, false);
        messagingTemplate.convertAndSendToUser(
            recipient.getId().toString(),
            "/chatroom-list",
            ""
        );
        try {
            List<ChatMessage> li = chatMessageService.findChatMessages(senderId, recipient.getId());
            return ResponseEntity.ok(
                new ResponseData(
                    true,
                    li,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }

    @GetMapping("/seen-message/{senderId}")
    public ResponseEntity<?> seenMessage(
        @PathVariable Long senderId,
        @AuthenticationPrincipal User recipient
    ) {
        try {
            chatRoomService.seenMessage(recipient.getId(), senderId, false);
            messagingTemplate.convertAndSendToUser(
                recipient.getId().toString(),
                "/chatroom-list",
                ""
            );
            return ResponseEntity.ok(
                new ResponseData(
                    true,
                    null,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }

    @GetMapping("/chat-room/all")
    public ResponseEntity<?> findChatRooms (@AuthenticationPrincipal User user) {
        try {
            List<ChatRoom> li = chatRoomService.findChatRooms(user.getId());
            return ResponseEntity.ok(
                new ResponseData(
                    true,
                    li,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }
}
