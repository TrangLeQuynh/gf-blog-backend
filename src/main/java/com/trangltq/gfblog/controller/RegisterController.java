package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.model.Verification;
import com.trangltq.gfblog.service.DefaultMailService;
import com.trangltq.gfblog.service.UserService;
import com.trangltq.gfblog.service.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/register")
@CrossOrigin
public class RegisterController {


    @Autowired
    private DefaultMailService mailService;

    @Autowired
    UserService userService;

    @Autowired
    VerificationService verificationService;

    public void sendMail(String email,String verifyCode) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom("grefireblog@gmail.com");
        mailMessage.setText(
                "Here is your verification code to continue\n"
                    + verifyCode
                    + "\n Please enter this code to verify your identity and sign in"
        );
        mailService.sendEmail(mailMessage);
    }

    @PostMapping("/send-verify")
    public ResponseEntity<?> confirmVerifyCode(@RequestBody Map<String, String> formData) {
        Optional<User> userOptional = userService.getUserByEmail(formData.get("email"));
        if (!userOptional.isPresent()) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_EMAIL_NOT_EXITS"
                )
            );
        }
        Verification verification = new Verification(userOptional.get());
        verificationService.save(verification);
        sendMail(formData.get("email"), verification.getVerifyCode());
        return ResponseEntity.ok().body(
            new ResponseData(
                true,
                verification.getId(),
                "MSG_SEND_MAIL_SUCCESS"
            )
        );

    }

    @PostMapping()
    public ResponseEntity<?> register(@RequestBody User user) {
        System.out.println("Register");
        if (userService.existsByEmail(user.getEmail()).isPresent()) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_EMAIL_EXIST"
                )
            );
        }
        User newUser = userService.addUser(
            new User(user.getUsername(), user.getEmail(), user.getPassword())
        );
        return ResponseEntity.ok().body(
            new ResponseData(
                true,
                 null,
                "MSG_REGISTER_SUCCESS"
            )
        );
    }

    @PostMapping("/confirm-verify")
    public ResponseEntity<?> confirmVerifyCode(@RequestBody Verification formData) {
        Optional<Verification> reps = verificationService.checkVerifyCode(formData.getId(), formData.getVerifyCode());
        if (!reps.isPresent()) {
            return ResponseEntity.ok(
                    new ResponseData(
                        false,
                        null,
                        "MSG_VERIFY_CODE_INCORRECT"
                    )
            );
        }
        Verification verification = reps.get();
        if (verification.isExpired()) {
            return ResponseEntity.ok(
                    new ResponseData(
                        false,
                        null,
                        "MSG_VERIFY_CODE_EXPIRED"
                    )
            );
        }
        //success
        User user = verification.getUser();
        user.setEnable(true);
        userService.editUser(user);
        return ResponseEntity.ok(
                new ResponseData(
                    true,
                    null,
                    "MSG_CONFIRM_SUCCESS"
                )
        );
    }

}
