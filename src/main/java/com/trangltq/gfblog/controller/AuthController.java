package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.JwtToken;
import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.Image;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.security.JwtTokenService;
import com.trangltq.gfblog.service.ImageService;
import com.trangltq.gfblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private UserService userService;

    @GetMapping
    public String hello() {
        return "Hello";
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Map<String,String> formData) {
        System.out.println("login");
        try {
            Optional<User> res = userService.findByEmail(formData.get("email"));
            if (!res.isPresent()) {
                return ResponseEntity.ok().body(
                    new ResponseData(
                        false,
                        null,
                        "MSG_EMAIL_NOT_EXITS"
                    )
                );
            }

            User newUser = res.get();
            System.out.println("new user");
            Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    newUser.getEmail(),
                    formData.get("password")
                )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtTokenService.generateJwtToken(authentication);
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            ResponseData responseData = new ResponseData(
                true,
                new JwtToken(jwt),
                "LOGIN_SUCCESS"
            );
            return ResponseEntity.ok().body(responseData);
        } catch (Exception e) {
            System.out.println(e.toString());
            return ResponseEntity.ok().body(
                new ResponseData(
                false,
                null,
                "MSG_LOGIN_FAIL"
                )
            );
        }
    }

}
