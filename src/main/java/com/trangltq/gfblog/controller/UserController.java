package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.service.ImageService;
import com.trangltq.gfblog.service.S3StorageService;
import com.trangltq.gfblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private S3StorageService s3StorageService;


    @GetMapping("/list/{username}")
    public ResponseEntity<?> getListByUsername(@PathVariable(name = "username") String username) {
        List<User> li = userService.findAllByUsername(username);
        return ResponseEntity.ok().body(
                new ResponseData(
                        true,
                        li,
                        "SUCCESS"
                )
        );
    }

    @GetMapping("/info")
    public ResponseEntity<?> getDetailUser(@AuthenticationPrincipal User userDetails) {
        User optionalUser = userService.getUserById(userDetails.getId());
        if (optionalUser == null) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_DONT_EXITS_USERNAME"
                )
            );
        }
        return ResponseEntity.ok().body(
            new ResponseData(
                    true,
                    optionalUser,
                    "SUCCESS"
            )
        );
    }

    @GetMapping("/{username}")
    public ResponseEntity<?> getDetailUser(@PathVariable(name = "username") String username) {
        Optional<User> optionalUser = userService.getUserByUsername(username);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "MSG_DONT_EXITS_USERNAME"
                    )
            );
        }
        return ResponseEntity.ok().body(
                new ResponseData(
                        true,
                        optionalUser.get(),
                        "SUCCESS"
                )
        );
    }

    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@RequestBody Map<String, String> formData, @AuthenticationPrincipal UserDetails userDetails) {
        Optional<User> optionalUser = userService.getUserByUsername(userDetails.getUsername());
        if (!optionalUser.isPresent()) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_SYSTEM_SOME_ERROR"
                )
            );
        }
        User newUser = optionalUser.get();
        newUser.setPassword(passwordEncoder.encode(formData.get("new_password")));
        newUser.setUpdateAt(new Timestamp(System.currentTimeMillis()));
        userService.editUser(newUser);
        return ResponseEntity.ok().body(
            new ResponseData(
                true,
                null,
                "MSG_CHANGE_PASS_SUCCESS"
            )
        );
    }

    @GetMapping("/find-new-friend")
    public ResponseEntity<?> getListPostAll(@AuthenticationPrincipal User userDetails, @RequestParam(name = "search") String search) {
        try {
            if (search.isEmpty()) {
                return  ResponseEntity.ok().body(
                    new ResponseData(
                        true,
                        null,
                        "SUCCESS"
                    )
                );
            }
            List<User> li = userService.findNewFriend(userDetails.getId(), search);
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    li,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_SYSTEM_SOME_ERROR"
                )
            );
        }
    }

    @PostMapping("/change-avatar")
    public ResponseEntity<?> uploadAvatar(@RequestBody String avatar, @AuthenticationPrincipal User userDetails) {
        try {
            String fileName = s3StorageService.uploadImage(avatar);
            if (fileName != null) {
                boolean res = userService.updateAvatar(userDetails.getId(), fileName);
                if (!res) {
                    return ResponseEntity.ok().body(
                        new ResponseData(
                            false,
                            null,
                            "MSG_UPDATE_FAIL"
                        )
                    );
                }
                if (userDetails.getAvatar() != null && userDetails.getAvatar().compareTo("") != 0) {
                    imageService.deleteImage(userDetails.getAvatar());
                    s3StorageService.deleteFile(userDetails.getAvatar());
                }
            }
            System.out.println("success change avatar");
            return ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    null,
                    "MSG_UPDATE_SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_UPDATE_FAIL"
                )
            );
        }
    }

    @PostMapping("/change-cover")
    public ResponseEntity<?> uploadCover(@RequestBody String cover, @AuthenticationPrincipal User userDetails) {
        try {
            String fileName = s3StorageService.uploadImage(cover);
            if (fileName != null) {
                boolean res = userService.updateCover(userDetails.getId(), fileName);
                if (!res) {
                    return ResponseEntity.ok().body(
                        new ResponseData(
                            false,
                            null,
                            "MSG_UPDATE_FAIL"
                        )
                    );
                }
                if (userDetails.getCover() != null && userDetails.getCover().compareTo("") != 0) {
                    imageService.deleteImage(userDetails.getCover());
                    s3StorageService.deleteFile(userDetails.getCover());
                }
            }
            return ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    null,
                    "MSG_UPDATE_SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_UPDATE_FAIL"
                )
            );
        }
    }

}
