package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.FriendData;
import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.Friend;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.service.FriendService;
import com.trangltq.gfblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/friend")
public class FriendController {

    @Autowired
    private FriendService friendService;

    @Autowired
    private UserService userService;

    @PostMapping("/add-friend")
    public ResponseEntity<?> requestAddFriend(@RequestBody FriendData friend) {
        try {
            friendService.addFriend(friend.getUserId(), friend.getFriendId(), "pending");
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    friend,
                    "MSG_REQUEST_SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }

    @PostMapping("/response-friend")
    public ResponseEntity<?> responseFriend(@RequestBody FriendData friend) {
        try {
            friendService.deleteFriend(friend.getUserId(), friend.getFriendId());
            friendService.deleteFriend(friend.getFriendId(), friend.getUserId());
            if (friend.getStatus()) {
                friendService.addFriend(friend.getUserId(), friend.getFriendId(),"friend");
                friendService.addFriend(friend.getFriendId(), friend.getUserId(),"friend");
            }
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    null,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }

    @GetMapping("/request-list")
    public ResponseEntity<?> getListRequest(@AuthenticationPrincipal User userDetails) {
        try {
            List<Friend> friendList = friendService.getListRequest(userDetails.getId(), "pending");
            return ResponseEntity.ok().body(
                    new ResponseData(
                        true,
                        friendList,
                        "SUCCESS"
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_SYSTEM_SOME_ERROR"
                )
            );
        }
    }

    @GetMapping("/friend-list")
    public ResponseEntity<?> getListFriend(@AuthenticationPrincipal User userDetails, @RequestParam(name = "search") String search) {
        try {
            List<Friend> friendList = friendService.getListFriend(userDetails.getId(), "friend", search);
            return ResponseEntity.ok().body(
                    new ResponseData(
                            true,
                            friendList,
                            "SUCCESS"
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "MSG_SYSTEM_SOME_ERROR"
                    )
            );
        }
    }

    @GetMapping("/account-friends/{userId}")
    public ResponseEntity<?> accountFriends(@PathVariable(name = "userId") Long userId) {
        try {
            Integer account = friendService.accountFriends(userId, "friend");
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    account,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    0,
                    "MSG_SYSTEM_SOME_ERROR"
                )
            );
        }
    }

}
