package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.PostData;
import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.Post;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping("/account-posts/{userId}")
    public ResponseEntity<?> accountPost(
        @PathVariable(name = "userId") Long userId
    ) {
        try {
            Integer account = postService.countPost(userId);
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    account,
                    "MSG_REQUEST_SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    0,
                    "MSG_SYSTEM_SOME_ERROR"
                )
            );
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDetailPost(@PathVariable(name = "id") Long id) {
        try {
            Optional<Post> optionalPost = postService.getDetailPost(id);
            if (!optionalPost.isPresent()) {
                new ResponseData(
                        false,
                        null,
                        "MSG_DONT_FIND_THIS_POST"
                );
            }
            return  ResponseEntity.ok().body(
                    new ResponseData(
                            true,
                            optionalPost.get(),
                            "SUCCESS"
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "MSG_SYSTEM_SOME_ERROR"
                    )
            );
        }
    }

    @GetMapping("/list-post/{userId}")
    public ResponseEntity<?> getListPost(
        @PathVariable(name = "userId") Long userId,
        @Param("page") Integer page,
        @Param("size") Integer size
    ) {
        try {
            System.out.println("get list");
            List<Post> li = postService.getPostsByUserId(userId, page, size);
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    li,
                    "MSG_REQUEST_SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_SYSTEM_SOME_ERROR"
                )
            );
        }
    }

    @GetMapping("/list-post-all")
    public ResponseEntity<?> getListPostAll(
        @AuthenticationPrincipal User userDetails,
        @Param("page") Integer page,
        @Param("size") Integer size
    ) {
        try {
            List<Post> li = postService.getPostList(userDetails.getId(), page, size);
            return  ResponseEntity.ok().body(
                new ResponseData(
                        true,
                        li,
                        "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "MSG_SYSTEM_SOME_ERROR"
                    )
            );
        }
    }

    @PostMapping("/add")
    public ResponseEntity<?> addPost(@AuthenticationPrincipal User userDetails, @RequestBody PostData post) {
        try {
            postService.addPost(userDetails.getId(), post);
            return  ResponseEntity.ok().body(
                new ResponseData(
                true,
                null,
                "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "MSG_SYSTEM_SOME_ERROR"
                    )
            );
        }
    }

    @PostMapping("/update")
    public ResponseEntity<?> updatePost(@RequestBody PostData post, @AuthenticationPrincipal User userDetails) {
        try {
            postService.updatePost(userDetails.getId(), post);
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    null,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "MSG_SYSTEM_SOME_ERROR"
                )
            );
        }
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deletePost(@PathVariable Long id) {
        try {
            boolean res = postService.deletePost(id);
            if (res) {
                return ResponseEntity.ok().body(
                    new ResponseData(
                        true,
                        null,
                        "SUCCESS"
                    )
                );
            }
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }
}
