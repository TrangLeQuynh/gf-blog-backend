package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.CommentData;
import com.trangltq.gfblog.json.PayloadData;
import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.ChatMessage;
import com.trangltq.gfblog.model.Comment;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.service.CommentService;
import com.trangltq.gfblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @GetMapping("/{id}")
    public ResponseEntity<?> getListComment(@PathVariable(value = "id") Long postId) {
        try {
            List<Comment> li = commentService.getListComment(postId);
            return ResponseEntity.ok().body(
                    new ResponseData(
                            true,
                            li,
                            "SUCCESS"
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "FAIL"
                    )
            );
        }
    }

    @MessageMapping("/comment-add")
    public ResponseEntity<?> requestAddComment(@Payload Map<String, String> data) {
        try {
            Comment newComment = new Comment();
            Long userOwnerID = Long.parseLong(data.get("ownerId"));
            newComment.setPostId(Long.parseLong(data.get("postId")));
            newComment.setContent(data.get("content"));
            newComment.setUser(userService.getUserById(Long.parseLong(data.get("userId"))));
            commentService.addComment(
                newComment.getPostId(),
                newComment.getUser().getId(),
                newComment.getContent()
            );
            if (userOwnerID != newComment.getUser().getId()) {
                PayloadData _newPayload = new PayloadData();
                _newPayload.setSenderName(newComment.getUser().getUsername());
                _newPayload.setId(newComment.getPostId());
                _newPayload.setType(2);
                messagingTemplate.convertAndSendToUser(
                    userOwnerID.toString(),
                    "/comment-list/notification",
                    _newPayload
                );
            }
            messagingTemplate.convertAndSendToUser(
                data.get("postId"),
                "/comment-post",
                newComment
            );
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    null,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "FAIL"
                    )
            );
        }
    }


    @MessageMapping("/comment-delete")
    public ResponseEntity<?> requestDeleteComment(@Payload Map<String, String> data) {
        try {
            Long id = Long.parseLong(data.get("id"));
            commentService.deleteCommentByID(id);
            messagingTemplate.convertAndSendToUser(
                data.get("postId"),
                "/delete/comment-post",
                data.get("id")
            );
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    null,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                new ResponseData(
                    false,
                    null,
                    "FAIL"
                )
            );
        }
    }

//    @MessageMapping("/chat")
//    public void processMessage(@Payload ChatMessage chatMessage) {
//        Optional<String> chatId = chatRoomService.getChatId(
//                chatMessage.getSenderId(),
//                chatMessage.getRecipientId(),
//                true
//        );
//        System.out.println("send chat message");
//        chatMessage.setChatId(chatId.get());
//        ChatMessage saved = chatMessageService.save(chatMessage);
//        messagingTemplate.convertAndSendToUser(
//                chatMessage.getRecipientId().toString(),
//                "/chatroom-list",
//                saved
//        );
//        messagingTemplate.convertAndSendToUser(
//                chatMessage.getRecipientId().toString(),
//                "/messages",
//                saved
//        );
//    }

}
