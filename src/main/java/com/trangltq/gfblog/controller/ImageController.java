package com.trangltq.gfblog.controller;

import com.trangltq.gfblog.json.ResponseData;
import com.trangltq.gfblog.model.Image;
import com.trangltq.gfblog.model.User;
import com.trangltq.gfblog.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/images")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @GetMapping
    public ResponseEntity<?> getYourImages(@AuthenticationPrincipal User userDetails, @Param("page") Integer page, @Param("size") Integer size) {
        try {
            List<String> li = imageService.getListImageByUserID(userDetails.getId(), page, size);
            return  ResponseEntity.ok().body(
                new ResponseData(
                    true,
                    li,
                    "SUCCESS"
                )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "MSG_SYSTEM_SOME_ERROR"
                    )
            );
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getImages(
            @PathVariable(name = "id") Long userID,
            @Param("page") Integer page,
            @Param("size") Integer size
    ) {
        try {
            List<String> li = imageService.getListImageByUserID(userID, page, size);
            return  ResponseEntity.ok().body(
                    new ResponseData(
                            true,
                            li,
                            "SUCCESS"
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.ok().body(
                    new ResponseData(
                            false,
                            null,
                            "MSG_SYSTEM_SOME_ERROR"
                    )
            );
        }
    }


}
