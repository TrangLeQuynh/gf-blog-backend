package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.Friend;
import com.trangltq.gfblog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@EnableJpaRepositories
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM User WHERE email = :email LIMIT 1", nativeQuery = true)
    Optional<User> findByEmail(@Param("email") String email);

    @Query(value = "SELECT * FROM User WHERE username = :account OR email = :account LIMIT 1", nativeQuery = true)
    Optional<User> findByUsernameOrEmail(@Param("account") String account);

    @Query(value = "SELECT * FROM User WHERE email = :email LIMIT 1", nativeQuery = true)
    Optional<User> existsByEmail(@Param("email") String email);

    @Query(value = "SELECT * FROM User WHERE email = :email LIMIT 1", nativeQuery = true)
    Optional<User> getUserByEmail(@Param("email") String email);

    @Query(value = "SELECT * FROM User WHERE username LIKE :username% LIMIT 15", nativeQuery = true)
    List<User> findAllByUsername(@Param("username") String username);

    @Query(value = "SELECT * FROM User WHERE username = :username", nativeQuery = true)
    Optional<User> getByUsername(@Param("username") String username);

    @Query(value = "SELECT * FROM user WHERE username LIKE :usernameSearch%"
            + " AND id NOT IN (SELECT friend_id FROM friend WHERE user_id = :userId"
            + " UNION SELECT user_id FROM friend WHERE friend_id = :userId)"
            + " AND id != :userId"
            + " LIMIT 15"
            , nativeQuery = true)
    List<User> findNewFriend(@Param("userId") Long userId, @Param("usernameSearch") String usernameSearch);

    @Query(value = "SELECT * FROM User WHERE username = :username AND password = :password", nativeQuery = true)
    Optional<User> checkOldPassword(@Param("username") String username, @Param("password") String password);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "UPDATE user SET avatar = :avatar WHERE id = :userId", nativeQuery = true)
    void updateAvatar(@Param("userId") Long userId, @Param("avatar") String avatar);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "UPDATE user SET cover = :cover WHERE id = :userId", nativeQuery = true)
    void updateCover(@Param("userId") Long userId, @Param("cover") String cover);


}
