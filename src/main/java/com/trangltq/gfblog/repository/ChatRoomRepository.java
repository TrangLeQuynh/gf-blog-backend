package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.ChatRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface ChatRoomRepository extends JpaRepository<ChatRoom, Long> {
    @Query(
        value = "SELECT chat_id FROM chat_room WHERE sender_id = :senderId AND recipient_id = :recipientId LIMIT 1",
        nativeQuery = true
    )
    Optional<String> findBySenderIdAndRecipientId(@Param("senderId") Long senderId, @Param("recipientId") Long recipientId);


    @Query(
        value = "SELECT * FROM chat_room WHERE sender_id = :userID ORDER BY update_at DESC",
        nativeQuery = true
    )
    List<ChatRoom> findChatRooms(@Param("userID") Long userID);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "INSERT INTO chat_room (chat_id, sender_id, recipient_id, status, update_at) VALUES (:chatId, :senderId, :recipientId, :status, :updateAt)", nativeQuery = true)
    void addChatRoom(
        @Param("chatId") String chatId,
        @Param("senderId") Long senderId,
        @Param("recipientId") Long recipientId,
        @Param("status") Boolean status,
        @Param("updateAt") LocalDateTime updateAt
    );

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(
        value = "UPDATE chat_room SET status = :status, update_at = :updateAt WHERE sender_id = :senderId AND recipient_id = :recipientId",
        nativeQuery = true
    )
    void updateStatus(
        @Param("senderId") Long senderId,
        @Param("recipientId") Long recipientId,
        @Param("status") Boolean status,
        @Param("updateAt") LocalDateTime updateAt
    );

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(
        value = "UPDATE chat_room SET status = :status WHERE sender_id = :senderId AND recipient_id = :recipientId",
        nativeQuery = true
    )
    void updateSeenStatus(
            @Param("senderId") Long senderId,
            @Param("recipientId") Long recipientId,
            @Param("status") Boolean status
    );

}
