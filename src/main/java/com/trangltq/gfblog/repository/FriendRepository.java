package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.Friend;
import com.trangltq.gfblog.model.FriendKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@EnableJpaRepositories
@Repository
public interface FriendRepository extends JpaRepository<Friend, FriendKey> {

    @Query(value = "(SELECT * FROM friend WHERE status = :status AND friend_id = :friendId ORDER BY create_at DESC)", nativeQuery = true)
    List<Friend> getRequestFriend(@Param("friendId") Long friendId, @Param("status") String status);

    @Query(value = "SELECT * FROM friend WHERE status = :status AND user_id = :userId"
            + " AND friend_id IN (SELECT id FROM user WHERE username LIKE %:usernameSearch%)"
            , nativeQuery = true)
    List<Friend> getListFriend(@Param("userId") Long userId, @Param("status") String status, @Param("usernameSearch") String usernameSearch);

    @Query(value = "SELECT * FROM friend WHERE status = :status AND user_id = :userId AND status = :status", nativeQuery = true)
    List<Friend> accountFriends(@Param("userId") Long userId, @Param("status") String status);

//    @Query(value = "SELECT * FROM friend WHERE status = :status AND user_id = :userId AND status = :status", nativeQuery = true)
//    List<Friend> get(@Param("userId") Long userId, @Param("status") String status);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "UPDATE friend f SET status = :status WHERE user_id = :userId AND friend_id = :friendId", nativeQuery = true)
    void updateStatusFriend(
        @Param("userId") Long userId,
        @Param("friendId") Long friend_id,
        @Param("status") String status);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "DELETE FROM friend WHERE user_id = :userId AND friend_id = :friendId", nativeQuery = true)
    void deleteFriend(@Param("userId") Long userId, @Param("friendId") Long friend_id);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "INSERT INTO friend (user_id, friend_id, create_at, status) VALUES (:userId, :friendId, :createAt, :status )", nativeQuery = true)
    void addFriend(@Param("userId") Long userId, @Param("friendId") Long friend_id, @Param("createAt") LocalDateTime createAt, @Param("status") String status);

}
