package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.Verification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("verificationReps")
public interface VerificationRepository extends JpaRepository<Verification, Long> {

    @Query(value = "SELECT * FROM verification" + " WHERE id = :id AND verify_code = :verifyCode", nativeQuery = true)
    Optional<Verification> checkVerification(@Param("id") Long id, @Param("verifyCode") String verifyCode);

}
