package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.Languages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface LanguagesRepository extends JpaRepository<Languages, String> {

    @Query(value = "SELECT key_word, en as trans FROM languages", nativeQuery = true)
    List<Languages> getLanguagesEN();

    @Query(value = "SELECT key_word, vi as trans FROM languages", nativeQuery = true)
    List<Languages> getLanguagesVI();
}
