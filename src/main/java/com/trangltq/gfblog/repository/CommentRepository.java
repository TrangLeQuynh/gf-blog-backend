package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@EnableJpaRepositories
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query(value = "SELECT * FROM comment WHERE post_id = :postId", nativeQuery = true)
    List<Comment> getCommentList(@Param("postId") Long postId);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "INSERT INTO comment (post_id, user_id, content, create_at, update_at)"
             +" VALUES (:postId, :userId, :content, :createAt, :updateAt)", nativeQuery = true)
    void addComment(
            @Param("postId") Long postId,
            @Param("userId") Long userId,
            @Param("content") String content,
            @Param("createAt") LocalDateTime createAt,
            @Param("updateAt") LocalDateTime updateAt);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "DELETE FROM comment WHERE post_id = :postId", nativeQuery = true)
    void deleteComment(@Param("postId") Long postId);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "DELETE FROM comment WHERE id = :commentId", nativeQuery = true)
    void deleteCommentByID(@Param("commentId") Long commentId);

}
