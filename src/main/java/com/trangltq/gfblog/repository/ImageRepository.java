package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@EnableJpaRepositories
@Repository
public interface ImageRepository extends JpaRepository<Image, String> {

    @Query(value = "SELECT id FROM image WHERE user_id = :userId ORDER BY create_at DESC LIMIT :limit OFFSET :offset", nativeQuery = true)
    List<String> findAllByUserId(@Param("userId") Long userId, @Param("offset") Integer offset, @Param("limit") Integer limit);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "INSERT INTO image (id, user_id, description, type, create_at)"
            + "VALUES (:id, :userId, :description, :type, :createAt)", nativeQuery = true)
    void saveImage(
        @Param("id") String id,
        @Param("userId") Long userId,
        @Param("description") String description,
        @Param("type") Integer type,
        @Param("createAt") LocalDateTime createAt
    );
}
