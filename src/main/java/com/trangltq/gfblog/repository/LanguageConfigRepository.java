package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.LanguageConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageConfigRepository extends JpaRepository<LanguageConfig, String> {
}
