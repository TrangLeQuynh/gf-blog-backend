package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Repository
@EnableJpaRepositories
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {

    @Query(
        value = "SELECT * FROM chat_message WHERE chat_id = :chatId",
        nativeQuery = true
    )
    List<ChatMessage> findAllByChatId(@Param("chatId") String chatId);


    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "INSERT INTO chat_message (chat_id, sender_id, recipient_id, content, create_at) "
            + "VALUES (:chatId, :senderId, :recipientId, :content, :createAt)", nativeQuery = true)
    void addChatMessage(
        @Param("chatId") String chatId,
        @Param("senderId") Long senderId,
        @Param("recipientId") Long recipientId,
        @Param("content") String content,
        @Param("createAt") LocalDateTime createAt
        );


}
