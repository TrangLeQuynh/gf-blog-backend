package com.trangltq.gfblog.repository;

import com.trangltq.gfblog.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query(
        value = "SELECT * FROM post WHERE user_id = :userId ORDER BY create_at DESC LIMIT :limit OFFSET :offset",
        nativeQuery = true)
    List<Post> getPostsByUserId(
        @Param("userId") Long userId,
        @Param("offset") Integer offset,
        @Param("limit") Integer limit
    );

    @Query(
            value = "SELECT COUNT(*) FROM post WHERE user_id = :userId",
            nativeQuery = true)
    Integer countPost(@Param("userId") Long userId);

    @Query(value = "SELECT * FROM post WHERE id = :id LIMIT 1", nativeQuery = true)
    Optional<Post> getPostDetail(@Param("id") Long id);

    @Query(value = "SELECT * FROM post"
            + " WHERE user_id = :userId OR user_id IN (SELECT friend_id FROM friend WHERE user_id = :userId AND status = :status) ORDER BY create_at DESC"
            + " LIMIT :limit OFFSET :offset"
            , nativeQuery = true)
    List<Post> getPostListAll(
            @Param("userId") Long userId,
            @Param("status") String status,
            @Param("offset") Integer offset,
            @Param("limit") Integer limit
    );

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "INSERT INTO post (user_id, content, hash_tag, images, create_at, update_at, latitude, longitude)"
            + "VALUES (:userId, :content, :hashTag, :images, :createAt, :updateAt, :latitude, :longitude)", nativeQuery = true)
    void addPost(
        @Param("userId") Long userId,
        @Param("content") String content,
        @Param("hashTag") String hashTag,
        @Param("images") String images,
        @Param("createAt") LocalDateTime createAt,
        @Param("updateAt") LocalDateTime updateAt,
        @Param("latitude") double latitude,
        @Param("longitude") double longitude
        );

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "UPDATE Post"
        + " SET content = :content, images = :images, update_at = :updateAt, latitude = :latitude, longitude = :longitude"
        + " WHERE id = :id", nativeQuery = true)
    void updatePost(
            @Param("id") Long id,
            @Param("content") String content,
            @Param("images") String images,
            @Param("updateAt") LocalDateTime updateAt,
            @Param("latitude") double latitude,
            @Param("longitude") double longitude
    );

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "DELETE FROM post WHERE id = :id", nativeQuery = true)
    void deletePost(@Param("id") Long id);


}
